package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	log "github.com/sirupsen/logrus"
	cm "gitlab.com/alamrk.informatic1/lemonilo_test/common"
	db "gitlab.com/alamrk.informatic1/lemonilo_test/db"
	sv "gitlab.com/alamrk.informatic1/lemonilo_test/services"
)

func initLogger() {
	log.SetFormatter(&log.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05.999",
	})
}

func initHandlers(e *echo.Echo) {
	r := e.Group(cm.Config.RootURL)

	r.GET("/ping", sv.Ping)
	r.POST("/login", sv.Login)

	// Configure middleware with the custom claims type
	config := middleware.JWTConfig{
		SigningKey: []byte(cm.Config.Secret),
	}
	r.Use(middleware.JWTWithConfig(config))

	r.GET("/user/:id", sv.GetUser)
	r.GET("/users", sv.GetAllUsers)
	r.POST("/users", sv.CreateUser)
	r.PUT("/user/:id", sv.UpdateUser)
	r.DELETE("/user/:id", sv.DeleteUser)

	log.Info("Start Server")
}

func main() {
	initLogger()
	e := echo.New()

	cm.LoadConfig()

	db.Db = db.MariaDBInit()

	initHandlers(e)

	e.Logger.Fatal(e.Start(cm.Config.ListenPort))

}
