package lib

import (
	"strconv"

	"github.com/labstack/echo/v4"
	cm "gitlab.com/alamrk.informatic1/lemonilo_test/common"
)

// swagger:model Pagination
type Pagination struct {
	Total        *uint64     `json:"total"`
	PerPage      uint64      `json:"per_page"`
	CurrentPage  uint64      `json:"current_page"`
	LastPage     *uint64     `json:"last_page"`
	FirstPageURL *string     `json:"first_page_url"`
	PrevPageURL  *string     `json:"prev_page_url"`
	NextPageURL  *string     `json:"next_page_url"`
	LastPageURL  *string     `json:"last_page_url"`
	From         uint64      `json:"from"`
	To           uint64      `json:"to"`
	Data         interface{} `json:"data,omitempty"`
	AccessRight  *[]string   `json:"access_right,omitempty"`
}

func GenerateMeta(c echo.Context, total uint64, limit uint64, page uint64, offset uint64, pagination bool, params map[string]string) Pagination {
	var meta Pagination
	queryParam := ""

	if len(params) > 0 {
		i := 0
		queryParam += "&"
		for index, query := range params {
			queryParam += index + "=" + query
			if (len(params) - 1) > i {
				queryParam += "&"
			}
			i++
		}
	}

	if pagination == true {
		meta.Total = &total
		meta.PerPage = limit
		lastPage := total / limit
		meta.LastPage = &lastPage
		if (total % limit) > 0 {
			*meta.LastPage++
		}
		if (offset + limit) > total {
			meta.To = total
		} else {
			meta.To = offset + limit
		}
		meta.From = offset + 1
		if page == 0 {
			meta.CurrentPage = 1
			meta.To = total
		} else {
			meta.CurrentPage = page
		}
	}

	if page > 1 {
		prevPageURL := cm.Config.Host + cm.Config.ListenPort + c.Path() + "?limit=" + strconv.FormatUint(limit, 10) + "&page=" + strconv.FormatUint(page-1, 10) + queryParam
		meta.PrevPageURL = &prevPageURL
		firstPageURL := cm.Config.Host + cm.Config.ListenPort + c.Path() + "?limit=" + strconv.FormatUint(limit, 10) + "&page=1" + queryParam
		meta.FirstPageURL = &firstPageURL
	}
	if pagination == true {
		if meta.CurrentPage < *meta.LastPage {
			nextPageURL := cm.Config.Host + cm.Config.ListenPort + c.Path() + "?limit=" + strconv.FormatUint(limit, 10) + "&page=" + strconv.FormatUint(meta.CurrentPage+1, 10) + queryParam
			meta.NextPageURL = &nextPageURL
			lastPageURL := cm.Config.Host + cm.Config.ListenPort + c.Path() + "?limit=" + strconv.FormatUint(limit, 10) + "&page=" + strconv.FormatUint(*meta.LastPage, 10) + queryParam
			meta.LastPageURL = &lastPageURL
		}
	} else {
		nextPageURL := cm.Config.Host + cm.Config.ListenPort + c.Path() + "?limit=" + strconv.FormatUint(limit, 10) + "&page=" + strconv.FormatUint(page+1, 10) + queryParam
		meta.NextPageURL = &nextPageURL
	}
	return meta
}

func Paginate(c echo.Context, data interface{}, total uint64, limit uint64, page uint64, offset uint64, pagination bool) Pagination {
	params := make(map[string]string)
	meta := GenerateMeta(c, total, limit, page, offset, pagination, params)
	meta.Data = data
	return meta
}
