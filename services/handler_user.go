package services

import (
	"encoding/base64"
	"log"
	"net/http"
	"strconv"

	cm "gitlab.com/alamrk.informatic1/lemonilo_test/common"
	"gitlab.com/alamrk.informatic1/lemonilo_test/lib"
	"gitlab.com/alamrk.informatic1/lemonilo_test/models"
	md "gitlab.com/alamrk.informatic1/lemonilo_test/models"

	"github.com/labstack/echo/v4"
)

func GetUser(c echo.Context) error {
	idStr := c.Param("id")
	id, _ := strconv.ParseUint(idStr, 10, 64)
	if id == 0 {
		return echo.NewHTTPError(http.StatusNotFound)
	}
	var user md.User
	status := md.GetUser(&user, idStr)
	log.Println(status)
	if status == http.StatusOK {
		return c.JSON(status, user)
	} else {
		return echo.NewHTTPError(status)
	}
}

func GetAllUsers(c echo.Context) error {
	var err error
	// Get parameter limit
	limitStr := c.QueryParam("limit")
	var limit uint64
	if limitStr != "" {
		limit, err = strconv.ParseUint(limitStr, 10, 64)
		if err == nil {
			if (limit == 0) || (limit > cm.Config.LimitQuery) {
				limit = cm.Config.LimitQuery
			}
		} else {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
	} else {
		limit = cm.Config.LimitQuery
	}
	// Get parameter page
	pageStr := c.QueryParam("page")
	var page uint64
	if pageStr != "" {
		page, err = strconv.ParseUint(pageStr, 10, 64)
		if err == nil {
			if page == 0 {
				page = 1
			}
		} else {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
	} else {
		page = 1
	}
	var offset uint64
	if page > 1 {
		offset = limit * (page - 1)
	}
	// Get parameter pagination
	pagination := false
	paginationStr := c.QueryParam("pagination")
	if paginationStr != "" {
		pagination, err = strconv.ParseBool(paginationStr)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
	}

	params := make(map[string]string)
	//Get parameter active
	activeStr := c.QueryParam("active")
	if activeStr != "" {
		var active bool
		active, err = strconv.ParseBool(activeStr)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
		if active == true {
			params["active"] = "1"
		} else {
			params["active"] = "0"
		}
	}

	var users []md.User
	total, err := md.GetAllUsers(&users, limit, offset, pagination, params)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError)
	}
	if total == 0 {
		return echo.NewHTTPError(http.StatusNotFound)
	}
	result := lib.Paginate(c, users, total, limit, page, offset, pagination)
	return c.JSON(http.StatusOK, result)

}

func CreateUser(c echo.Context) error {
	params := make(map[string]string)

	// Get parameter email
	email := c.FormValue("email")
	if email != "" {
		params["email"] = email
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter address
	address := c.FormValue("address")
	if address != "" {
		params["address"] = address
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter username
	username := c.FormValue("username")
	if username != "" {
		params["username"] = username
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter password
	password := c.FormValue("password")
	if password != "" {
		var passwordEncode = base64.StdEncoding.EncodeToString([]byte(password))
		params["password"] = passwordEncode
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter address
	activeStr := c.FormValue("active")
	if activeStr != "" {
		order, _ := strconv.ParseUint(activeStr, 10, 64)
		if order > 0 {
			params["active"] = activeStr
		} else {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	status := models.CreateUser(params)
	return echo.NewHTTPError(status)
}

func UpdateUser(c echo.Context) error {
	params := make(map[string]string)

	//Get parameter id
	idStr := c.Param("id")
	id, _ := strconv.ParseUint(idStr, 10, 64)
	if id > 0 {
		params["id"] = strconv.FormatUint(id, 10)
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter email
	email := c.FormValue("email")
	if email != "" {
		params["email"] = email
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter address
	address := c.FormValue("address")
	if address != "" {
		params["address"] = address
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter username
	username := c.FormValue("username")
	if username != "" {
		params["username"] = username
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter password
	password := c.FormValue("password")
	if password != "" {
		var passwordEncode = base64.StdEncoding.EncodeToString([]byte(password))
		params["password"] = passwordEncode
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	// Get parameter address
	activeStr := c.FormValue("active")
	if activeStr != "" {
		order, _ := strconv.ParseUint(activeStr, 10, 64)
		if order > 0 {
			params["active"] = activeStr
		} else {
			return echo.NewHTTPError(http.StatusBadRequest)
		}
	} else {
		return echo.NewHTTPError(http.StatusBadRequest)
	}

	status := models.UpdateUser(params)
	return echo.NewHTTPError(status)
}

func DeleteUser(c echo.Context) error {
	idrStr := c.Param("id")
	id, _ := strconv.ParseUint(idrStr, 10, 64)
	if id == 0 {
		return echo.NewHTTPError(http.StatusBadRequest)
	}
	status := models.DeleteUser(strconv.FormatUint(id, 10))
	return echo.NewHTTPError(status)
}
