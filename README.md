# lemonilo_test

INTRODUCTION
------------
* link : https://gitlab.com/alamrk.informatic1/lemonilo_test
* DOCUMENTATION API postman collection (folder /doc/lemonilo_test.postman_collection)
* CAPTURE RESPONSE API (folder /doc/capture)
* DATABASE (folder /scripts/lemonilo.sql)

INSTALLATION, CONFIGURATION AND RUNING
------------

 * Import database from folder /scripts/swapbackendtest.sql
 * Config from /common/config.go
 * GO BUILD
 * ./lemonilo_test
 * Import postman collection FROM /doc/lemonilo_test.postman_collection
 
 TESTING
------------
* authorization (email: alamrk.informatic@gmail.com, password: Lemonilotest)
* example token from api login
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFsYW1yay5pbmZvcm1hdGljQGdtYWlsLmNvbSIsImV4cCI6MTYzMzc2NDM0NiwicGFzc3dvcmQiOiJUR1Z0YjI1cGJHOTBaWE4wIn0.-dC5PXznlqO5YufDkVEqVMDF2FHaL1h6HLbHpwiZAAw",
    "token_type": "bearer",
    "expires_in": 3600
}
* example Get All Users
{
    "total": null,
    "per_page": 0,
    "current_page": 0,
    "last_page": null,
    "first_page_url": null,
    "prev_page_url": null,
    "next_page_url": ":http://localhost:9673/lemonilotest/users?limit=100&page=2",
    "last_page_url": null,
    "from": 0,
    "to": 0,
    "data": [
        {
            "id": 1,
            "email": "alamrk.informatic@gmail.com",
            "address": "Jalan nam nam 2 no 5",
            "username": "alamrk.informatic",
            "password": "TGVtb25pbG90ZXN0",
            "active": "1"
        },
        {
            "id": 2,
            "email": "rachmat.kurniawan@gmail.com",
            "address": "Jalan kasturi 2 no 3",
            "username": "rachmat.kurniawan",
            "password": "TGVtb25pbG90ZXN0",
            "active": "0"
        }
    ]
}