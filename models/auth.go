package models

type OAuthMessage struct {
	AccessToken string `json:"access_token,omitempty" validate:"omitempty,max=255,min=1"`
	TokenType   string `json:"token_type,omitempty" validate:"omitempty,max=10,min=1"`
	ExpiresIn   uint64 `json:"expires_in,omitempty"`
}
