module gitlab.com/alamrk.informatic1/lemonilo_test

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.6.1
	github.com/sirupsen/logrus v1.8.1
)
