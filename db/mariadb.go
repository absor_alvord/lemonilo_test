package db

import (
	cm "gitlab.com/alamrk.informatic1/lemonilo_test/common"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var Db *sqlx.DB

func MariaDBInit() *sqlx.DB {
	db := sqlx.MustConnect("mysql", cm.Config.MariadbUser+":"+cm.Config.MariadbPassword+"@tcp("+cm.Config.MariadbHost+":"+cm.Config.MariadbPort+")/"+cm.Config.MariadbDb+"?charset=latin1")
	return db
}
